# SlkStrongsGreek

Tento projekt má za cieľ primárne vytvoriť Slovenský slovník Gréckych Strongových čísel, tak aby bol dostupný bez akýchkoľvek licenčných obmedzení (public domain).
Z tohto dôvodu nekopírujeme dáta z iných slovníkov resp. zdrojov vydaných pod inou licenciou.
Prvým míľnikom v tomto smere je preloženie do slovenčiny voľne dostupnej anglickej verzie `Strong's Greek Dictionary` [strongs-dictionary-xml](https://github.com/morphgnt/strongs-dictionary-xml/).

Vysvetlenie, na čo sú dobré strongové čísla, nájdete napríklad v tomto článku [Strongova čísla aneb biblické jazyky pro každého](https://revue.theofil.cz/revue-clanek.php?clanek=399).

## Chcem pomôcť

V prípade, že sa chcete zapojiť, najskôr pošlite e-mail na `zdposter@gmail.com` a v predmete e-mailu uveďte `SlkStrongsGreek`. Prekladať začnite až dohode s koordinátorom projektu, aby ste nerobili zbytočnú prácu.

Keďže `Strong's Greek Dictionary` vznikol v roku 1890, tak tomu zodpovedá aj angličtina. Nie vždy je jasné, ako slovo preložiť do slovenčiny. Napríklad pri slove `Μάρθα` je uvedené, že jeho význam je `mistress`, ktoré sa do slovenčiny dá preložiť dvoma výrazne rôznymi spôsobmi: a) milenka, b) pani.
 V takýchto nejasných prípadoch je dobré overiť správnosť prekladu v iných dostupných zdrojoch (napr. preklad Strong's Greek Dictionary do nemčiny alebo ruštiny, prípadne aj ďalšími zdrojmi v tomto prípade pomohli napr. https://www.collinsdictionary.com/dictionary/english/martha alebo https://www.dictionary.com/browse/martha)

Treba mať na pamäti, že nie vždy je možné/ľahké všetko preložiť do slovenčiny z angličtiny (napr. slovenčina nerozlišuje medzi `Mister/Sir/Lord` a použite na to jedno slovo: pán) a preto neprekladáme otrocky, ale tak, aby čitateľ pochopil význam slova.
Pri nejasných slovách je dobré pred ukončením prekladu:

1. Pohľadať pôvodný (grécky) význam slova (na internete je viacero zdrojov) a podľa neho upraviť slovenský preklad.
2. Pozrieť sa slovo preložili slovenský (napr. na [biblia.sk/](https://biblia.sk/citanie/seb/jk/1/5) je dostupných viacero najpoužívanejších slovenských prekladov online) prípadne český prekladatelia biblie. Android aplikácia  [AndBible: Bible Study](https://andbible.github.io/) má k tomu dobrú pomôcku: ku každému strongovémú číslu vie vyhľadať všetky výskyty daného čísla v preklade, ktorý obsahuje strongové čísla (najčastejšie asi KJV alebo NASB)

### Ukážka prekladu a ako používať značky

```xml
   <entryFree n="03007" value="beta:LEI/PW" type="translated">
    <orth>λείπω</orth>
    <orth type="trans" rend="bold">leípō,</orth>
    <pron rend="italic">li'-po</pron>
    <etym>primárne sloveso;</etym>
    <def>1. chýbať, niečo čo nie prítomné, nedostávať
    <lb/>2. byť chudobný, mať nedostatok, potrebovať
    <lb/><seg type="x-kjv-usage">:--čo ešte chýba, chýbať nedostávať; mať nedostatok.</seg>
    </def>
   </entryFree>
```

* jedno Strongové číslo je zatvorené v značke `entryFree`, pričom jeho číslo je uložené do atribútu `n` vo formáte `#####` (čiže s úvodnými nulami)
* pokiaľ je preklad Strongového čísla ukončený, tak pridáme do neho atribút `type="translated"` (tento atribút sa nezobrazuje vo výslednom module)
* atribúty `value="..."`, značky `orth`, `pron` nechávame bez zmeny
* prekladajú sa anglické časti v značkách `etym` (etymológia slova) a `def` (definícia slova)
* v (pod)značke `seg type="x-kjv-usage"` sa uvádza, ako je dané slovo preložené v Slovenskom Ekumenickom Preklade. Ako je slovo preložené v iných prekladoch je možné uviesť pred touto značkou (v časti `def`). V prípade, že slovo má viac významov, slová/preklady sa zoskupia do synonymických skupín, ktoré sa oddelia bodkočiarkou (`;`)
* značka `lb/` je interpretovaná ako nový riadok a slúži pre zlepšenie čitateľnosti vo výslednom module


## Licencia

Dáta sú k dispozícií pod licenciou [CC0 1.0 Universal (CC0 1.0) Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/)

## Stav projektu

Projekt je úvodnej fáze.

### Verzie:

 * **0.1-220919**:
    * vytvorenie css štýlu, ktorý umožní "pekné" zobrazenie xml dát v moderných prehliadačoch
    * zarovnanie elementov v xml súbore, aby sa ľahšie robil preklad
    * vytvorenie python3 skriptu na vygenerovanie imp verzie slovníku, vhodného na generovanie slovníka pre [The Sword project](http://crosswire.org/)
    * preloženie pár desiatok slov a vygenerovanie testovanie *The Sword modulu* na otestovanie v programoch [Xiphos](https://xiphos.org/), [BibleTime](https://bibletime.info/) and [AndBible: Bible Study](https://andbible.github.io/)
