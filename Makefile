
bakfile=SlkStrongsGreek-$(shell date +%Y%m%d).zip
#Use add path to Windows tools on Windows
ifeq ($(OS),Windows_NT)
	export SWORD_PATH:="F:\win64.old\share\sword"
	export PATH := ${CURDIR}/wintools;$(PATH)
	XMLLINT_CMD := "F:\vcpkg\downloads\tools\perl\c\bin\xmllint.exe"
	TEI2MOD_CMD := ${CURDIR}/wintools/tei2mod.exe
	INSTALLMGR_CMD := ${CURDIR}/wintools/installmgr.exe
	ZIP_CMD := ${CURDIR}/wintools/zip.exe
else
	XMLLINT_CMD := xmllint
	TEI2MOD_CMD := tei2mod
	INSTALLMGR_CMD := installmgr
	ZIP_CMD := zip
endif

install:
	$(TEI2MOD_CMD) SlkStrongsGreek/modules/lexdict/zld/slkstrongsgreek SlkStrongsGreek.tei.xml -z z
	$(INSTALLMGR_CMD) -li SlkStrongsGreek SlkStrongsGreek

uninstall:
	$(INSTALLMGR_CMD) -u SlkStrongsGreek

validate:
	$(XMLLINT_CMD) --noout --relaxng tei_all.rng SlkStrongsGreek.tei.xml

zip:
	cd SlkStrongsGreek; \
	$(ZIP_CMD) -r ../$(bakfile) * -x *.cache

dist: install zip

