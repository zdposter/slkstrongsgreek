#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    Script to generate StongsGreek modul from Thml formated xml.

    Usage:
        python3 create_module_from_imp.py PROJECT_NAME
"""

import datetime
import os
import pathlib
import shutil
import subprocess
import sys
import zipfile

import lxml.etree as etree

## Configuration
PROJECT = "SlkStrongsGreek"
GREEK_DICT = PROJECT  # "StrongsRealGreek"
HEBR_DICT = "StrongsRealHebrew"
SOURCE = "strongsgreek_sk.xml"
MODDRV = "RawLD4"  # 'zText'
MODTYPE = "lexdict"  # 'texts'
SWORD_PATH = r"F:\win64\share\sword"
IMP2LD = r"F:\win64\bin\imp2ld.exe"
INSTALLMGR = r"F:\win64\bin\installmgr.exe"


conf_template = """[{}]
DataPath={}
ModDrv={}
Lang=sk
Feature=GreekDef
SwordVersionDate=2015-07-04
Version=0.1-220919
History_0.1-220919=Initial version (build test).
Encoding=UTF-8
Font=FreeSerif
SourceType=ThML
Description=Slovenský slovník gréckych Strongs čísel
About=Slovenský preklad anglického slovníku Strongs Real Greek Bible Dictionary
ShortPromo=<a href="https://gitlab.com/zdposter/slkstrongsgreek">SLKStrongsGreek repozitár</a>
TextSource=https://gitlab.com/zdposter/slkstrongsgreek
LCSH=Greek language--Dictionaries--Slovak.
DistributionLicense=Public Domain
Generated={}
InstallSize={}
"""


def parse_strongsref(item):
    """Create link to strong dictionary
    item.tag == 'strongsref'
    """
    link = ""
    language = item.attrib["language"]
    l_strongs = item.attrib["strongs"]
    if language == "GREEK":
        link = f' <a href="sword://{GREEK_DICT}/{l_strongs.zfill(5)}">{l_strongs}</a>'
    elif language == "HEBREW":
        link = f' <a href="sword://{HEBR_DICT}/{l_strongs.zfill(5)}">{l_strongs}</a>'
    return link


def parse_greek(item):
    """Create greek transcriptions of strong word
    item.tag == 'greek'
    """
    output = ""
    beta = item.attrib["BETA"]
    unicode = item.attrib["unicode"]
    translit = item.attrib["translit"]
    output += f" <b>{unicode}</b> [{beta}]" + " {" + f"{translit}" + "} "
    return output


def parse_pronunciation(item):
    """Create pronunciation of strong word
    item.tag == 'pronunciation'
    """
    return f"\<i>{item.attrib['strongs']}</i>\\"


def convert_thml_to_imp(input_name, output_name):
    tree = etree.parse(input_name)
    with open(output_name, "w", encoding="utf-8", newline="\n") as f:
        for entry in tree.xpath("//strongsdictionary/entries/entry"):
            entry_id = entry.attrib["strongs"]
            output = f"$$${entry_id}\n"
            for child in entry:
                # print(child.tag)
                if child.tag == "strongs":
                    output += f'<a name="{entry_id}">{child.text}</a>'
                elif child.tag == "greek":
                    output += parse_greek(child)
                elif child.tag == "pronunciation":
                    output += parse_pronunciation(child)
                # elif child.tag == "strongs_derivation":
                # output += f"<br/>\n{child.text.strip()}"
                elif child.tag in ["strongs_derivation", "strongs_def", "kjv_def"]:
                    output += f"<br/>\n{child.text.strip()}"
                elif child.tag == "strongsref":
                    output += parse_strongsref(child)
                else:
                    print(f"Unexpected tag: '{child.tag}' at {entry_id}")
                    output += f"<{child.tag}>{child.text}</{child.tag}>"
                if child.tail and child.tail.strip():
                    output += f" {child.tail.strip()} "
                for item in child.getchildren():
                    if item.tag == "strongsref":
                        output += parse_strongsref(item)
                    elif item.tag == "greek":
                        output += parse_greek(item)
                    elif item.tag == "pronunciation":
                        output += parse_pronunciation(item)
                    else:
                        print(f"Unexpected tag: '{item.tag}' at {entry_id}")
                        output += f"<{item.tag}>{item.text}</{item.tag}>"
                    if item.tail and item.tail.strip():
                        output += f" {item.tail.strip() }"
            # fix typography
            output = (
                output.replace("  ", " ")
                .replace("\n ", "\n")
                .replace(" ,", ",")
                .replace(" .", ".")
                .replace(" ;", ";")
                .replace(" )", ")")
                .replace(" :", ":")
                .replace(" ]", "]")
                .replace("( <", "(<")
                .replace('> "', '>"')
            )
            f.write(output + "\n")


def get_file_sizes(mods_path):
    """Calculate size of installed module"""
    size = 0
    files = pathlib.Path(mods_path).rglob("*")
    for file in files:
        if file.is_file():
            size += pathlib.Path(file).stat().st_size
    return size


def cmd_exists(cmd, alternative=None):
    """Check if command is in the path.
    If not use alternative."""
    if shutil.which(cmd):
        return cmd
    return alternative


def run_command(arguments, debug=True, limit=15):
    p = subprocess.Popen(
        args=arguments, shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )
    stdout_message, more_messages = p.communicate()
    if debug:
        stdout_messages = stdout_message.decode("utf-8").splitlines()
        if len(stdout_messages) > limit:
            print(stdout_messages[0:limit])
            print(stdout_messages[-limit:])
        else:
            print("\n".join(stdout_messages))
    if more_messages:
        print("more messages:", more_messages.decode("utf-8"))


def create_conf_file(project, MODTYPE, MODDRV):
    """Generate conf file for project."""
    conf_path = pathlib.Path(project, "mods.d", f"{project.lower()}.conf")

    InstallSize = get_file_sizes(project)
    print(f"project: {project}\nInstallSize: {InstallSize}")
    datapath_conf = f"./modules/{MODTYPE.lower()}/{MODDRV.lower()}/{project.lower()}"
    today_str = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    with open(conf_path, encoding="utf-8", mode="wt") as f_conf:
        f_conf.write(
            conf_template.format(project, datapath_conf, MODDRV, today_str, InstallSize)
        )


def create_zip_package(project):
    """Create zipped module for distribution"""
    pathlib.Path("module").mkdir(parents=True, exist_ok=True)
    zipObj = zipfile.ZipFile(f"module/{project}.zip", "w")
    for filename in pathlib.Path(project).rglob("*"):
        if filename.is_dir():
            continue
        filePath = pathlib.Path(*filename.parts[1:])
        zipObj.write(str(filename), str(filePath), compress_type=zipfile.ZIP_DEFLATED)
        print("Created package:", filename, filePath)
    zipObj.close()


def main():
    """Main loop"""
    if len(sys.argv) > 1:
        project = sys.argv[1]
    else:
        project = PROJECT
    datapath = pathlib.Path(
        project, "modules", MODTYPE.lower(), MODDRV.lower(), project.lower()
    )
    datapath.mkdir(parents=True, exist_ok=True)
    pathlib.Path(project, "mods.d").mkdir(parents=True, exist_ok=True)

    if not os.environ.get("SWORD_PATH", None):
        os.environ["SWORD_PATH"] = SWORD_PATH

    target = f"{project.lower()}.imp"
    # Convert xml to imp
    convert_thml_to_imp(SOURCE, target)

    # Generate Sword module from imp
    command = cmd_exists("imp2ld", IMP2LD)
    arguments = [command, target, "-4", "-o", str(datapath)]
    print(" ".join(arguments))
    run_command(arguments)
    create_conf_file(project, MODTYPE, MODDRV)

    # Before installing new version, old version must be uninstalled
    command = cmd_exists("installmgr", INSTALLMGR)
    arguments = [command, "-u", f"{project}"]
    print(" ".join(arguments))
    run_command(arguments)

    # Install (unzipped) module
    command = cmd_exists("installmgr", INSTALLMGR)
    arguments = [command, "-li", f"{project}/", f"{project}"]
    print(" ".join(arguments))
    run_command(arguments)

    # Create distribution zip
    create_zip_package(project)


if __name__ == "__main__":
    main()
