# SlkStrongsGreek

Slovak translation of  Strong's Greek Dictionary in XML with real Greek based on [strongs-dictionary-xml](https://github.com/morphgnt/strongs-dictionary-xml/)

Instruction in Slovak can be found in separated file [README.sk.md](README.sk.md).

Inštrukcie v slovenčine nájdete v samostatnom súbore [README.sk.md](README.sk.md).

## License

Released under [CC0 1.0 Universal (CC0 1.0) Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/)

## Project status

Project is in initial stage.
